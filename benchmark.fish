#!/usr/bin/env fish
set packages (paclist core | cut -f1 -d' ')
set --erase IFS
set parallel 10
set mirror 'http:\/\/ftp.snt.utwente.nl\/pub\/os\/linux\/archlinux\/$repo\/os\/$arch'

# setup the config
set config (cat pacman.conf | sed \
-e "s/Include = \/etc\/pacman.d\/mirrorlist/Server = $mirror/" \
-e "s/\#ParallelDownloads = 5/ParallelDownloads = $parallel/"
)
echo Testing with $parallel parallel and using $mirror

mkdir -p cache
time sudo build/pacman -S --noconfirm --downloadonly --config (echo $config | psub) --cache ./cache $packages
sudo rm cache/*
